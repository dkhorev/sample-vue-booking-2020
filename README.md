# Frontend | Booking app (test job) 01/2020

![Тесты](docs/front1.png "Тесты")

![Тесты](docs/front2.png "Тесты")

![Тесты](docs/front3.png "Тесты")

#### Установка

`cp .env.example .env.development`

добавить свой API ключ сервиса MapBox https://www.mapbox.com/

![Тесты](docs/mapbox.png "Тесты")

`npm install`

`npm run serve`

#### Тесты

Не писал, т.к. не было ранее такого опыта с Vue / фронтом. Есть примеры тестов для Node.Js + Jest.

