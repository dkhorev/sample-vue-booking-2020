export default {
  apiPath: process.env.VUE_APP_API_PATH,
  mapsApiKey: process.env.VUE_APP_MAPS_API_KEY,
};
