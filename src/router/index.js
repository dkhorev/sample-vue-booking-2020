import Vue from 'vue';
import VueRouter from 'vue-router';
import Events from '../views/Events.vue';
import EventSeats from '../views/EventSeats.vue';
import BookSeat from '../views/BookSeat.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Events',
    component: Events,
  },
  {
    path: '/place/:place/event/:event',
    name: 'EventSeats',
    component: EventSeats,
  },
  {
    path: '/place/:place/event/:event/book/:seat',
    name: 'BookSeat',
    component: BookSeat,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
