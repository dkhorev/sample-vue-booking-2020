import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {
  GET_PLACES,
  GET_SEATS,
  BOOK_SEAT,
  LOADING,
  REQUEST_LAST_ERROR,
  CLEAR_SEATS,
  CLEAR_REQUEST_LAST_ERROR, RESET_BOOKING_STATE,
} from './actions';
import getUrl from './utils';

Vue.use(Vuex);

axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
axios.defaults.headers.common.accept = 'application/json';

export default new Vuex.Store({
  state: {
    places: {
      data: [],
    },
    seats: {
      data: [],
    },
    loading: {},
    requestErrors: {},
    bookingFinished: false,
    path: {
      [GET_PLACES]: 'api/v1/places',
      [GET_SEATS]: 'api/v1/events/:event/seats',
      [BOOK_SEAT]: 'api/v1/seats/:seat',
    },
  },

  getters: {
    bookingFinished: state => state.bookingFinished,
    places: state => state.places.data,
    seats: state => state.seats.data,
    loading: state => state.loading,
    requestErrors: state => state.requestErrors,
  },

  mutations: {
    [LOADING]: (state, { name, value }) => {
      Vue.set(state.loading, name, value);
    },

    [REQUEST_LAST_ERROR]: (state, { name, value }) => {
      Vue.set(state.requestErrors, name, value);
    },

    [GET_PLACES]: (state, { data }) => {
      Vue.set(state.places, 'data', data.data);
    },

    [GET_SEATS]: (state, { data }) => {
      Vue.set(state.seats, 'data', data.data);
    },

    [CLEAR_SEATS]: (state) => {
      Vue.set(state.seats, 'data', []);
    },

    [BOOK_SEAT]: (state) => {
      Vue.set(state, 'bookingFinished', true);
    },

    [RESET_BOOKING_STATE]: (state) => {
      Vue.set(state, 'bookingFinished', false);
    },
  },

  actions: {
    [GET_PLACES]: async ({ state, commit }) => {
      commit(LOADING, { name: GET_PLACES, value: true });
      commit(REQUEST_LAST_ERROR, { name: GET_PLACES, value: false });

      try {
        const response = await axios.get(getUrl(state.path[GET_PLACES]));
        commit(GET_PLACES, response);
      } catch (e) {
        commit(REQUEST_LAST_ERROR, { name: GET_PLACES, value: e.response.data });
      }

      commit(LOADING, { name: GET_PLACES, value: false });
    },

    [GET_SEATS]: async ({ state, commit }, { eventId }) => {
      commit(LOADING, { name: GET_SEATS, value: true });
      commit(REQUEST_LAST_ERROR, { name: GET_SEATS, value: false });

      try {
        const response = await axios.get(getUrl(state.path[GET_SEATS], { event: eventId }));
        commit(GET_SEATS, response);
      } catch (e) {
        commit(REQUEST_LAST_ERROR, { name: GET_SEATS, value: e.response.data });
      }

      commit(LOADING, { name: GET_SEATS, value: false });
    },

    [CLEAR_SEATS]: async ({ commit }) => {
      commit(CLEAR_SEATS);
    },

    [CLEAR_REQUEST_LAST_ERROR]: async ({ commit }, {name}) => {
      commit(REQUEST_LAST_ERROR, { name, value: false });
    },

    [RESET_BOOKING_STATE]: async ({ commit }) => {
      commit(RESET_BOOKING_STATE);
    },

    [BOOK_SEAT]: async ({ state, commit }, { seat, formData }) => {
      commit(LOADING, { name: BOOK_SEAT, value: true });
      commit(REQUEST_LAST_ERROR, { name: BOOK_SEAT, value: false });

      try {
        const path = getUrl(state.path[BOOK_SEAT], { seat });
        const response = await axios.post(path, formData);
        commit(BOOK_SEAT, response);
      } catch (e) {
        commit(REQUEST_LAST_ERROR, { name: BOOK_SEAT, value: e.response });
      }

      commit(LOADING, { name: BOOK_SEAT, value: false });
    },
  },

  modules: {},
});
