import config from '../../config';

export default (url, data = {}) => {
  let path = url;
  path = replaceStringParams(path, data);

  return `${config.apiPath}${path}`
};

function replaceStringParams(url, data) {
  const regex = new RegExp(':(' + Object.keys(data).join('|') + ')', 'g');

  return url.replace(regex, (m, $1) => data[$1] || m);
}

